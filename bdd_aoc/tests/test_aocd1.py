"""Aoc Day one Part one feature tests."""

from pytest_bdd import (
    given,
    scenario,
    then,
    when,
)

@scenario('features\aoc_d1p1.feature', 'figure out frequenzy')
def test_figure_out_frequenzy():
    """figure out frequenzy."""


@given('a puzzle input')
def a_puzzle_input():
    """a puzzle input."""


@when('add numbers together from puzzle input')
def add_numbers_together_from_puzzle_input():
    """add numbers together from puzzle input."""


@then('return correct frequenzy')
def return_correct_frequenzy():
    """return correct frequenzy."""
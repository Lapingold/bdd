""" AoC 2018, day-1-part-1"""
def gettot():
    total = 0

    with open("input_data/day1.txt") as inp:
        for line in inp:
            try:
                num = int(line)
                total += num
            except ValueError:
                print('{} is not a number!'.format(line))

    print('Total of all numbers: {}'.format(total))
    return total










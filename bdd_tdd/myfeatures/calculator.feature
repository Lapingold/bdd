Feature: Test Calculator Functionality

    Testing a Calculator inside the Feature
    Scenario: Addition
        Given Calculator app is running
        When I input "2+3" to Calculator
        Then I get result "5"
    
    Scenario: Subtraction
        Given Calculator app is running
        When I input "5-2" to Calculator
        Then I get result "3"

    Scenario: Multiplication
        Given Calculator app is running
        When I input "5*2" to Calculator
        Then I get result "10"

    Scenario: Division
        Given Calculator app is running
        When I input "6/2" to Calculator
        Then I get result "3"

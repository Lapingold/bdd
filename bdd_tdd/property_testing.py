class Triangle:
    def __init__(self, base, height):
        self.base = base
        self.height = height
        @property
        def area(self):
            return self.base * self.height / 2
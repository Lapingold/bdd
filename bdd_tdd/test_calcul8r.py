"""Test Calculator Functionality feature tests."""

from pytest_bdd import (
    given,
    scenario,
    then,
    when,
)


@scenario('myfeatures\calculator.feature', 'Addition')
def test_addition():
    """Addition."""


@scenario('myfeatures\calculator.feature', 'Division')
def test_division():
    """Division."""


@scenario('myfeatures\calculator.feature', 'Multiplication')
def test_multiplication():
    """Multiplication."""


@scenario('myfeatures\calculator.feature', 'Subtraction')
def test_subtraction():
    """Subtraction."""


@given('Calculator app is running')
def calculator_app_is_running():
    """Calculator app is running."""


@when('I input "2+3" to Calculator')
def i_input_23_to_calculator():
    """I input "2+3" to Calculator."""


@when('I input "5*2" to Calculator')
def i_input_52_to_calculator():
    """I input "5*2" to Calculator."""


@when('I input "5-2" to Calculator')
def i_input_52_to_calculator():
    """I input "5-2" to Calculator."""


@when('I input "6/2" to Calculator')
def i_input_62_to_calculator():
    """I input "6/2" to Calculator."""


@then('I get result "10"')
def i_get_result_10():
    """I get result "10"."""


@then('I get result "3"')
def i_get_result_3():
    """I get result "3"."""


@then('I get result "5"')
def i_get_result_5():
    """I get result "5"."""


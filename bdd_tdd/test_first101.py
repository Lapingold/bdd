"""Bank Transactions feature tests."""

import pytest

from pytest_bdd import (
    given,
    scenario,
    then,
    when,
)

def pytest_configure():
    pytest.AMT = 0

@scenario('myfeatures\first101.feature', 'Withdrawal of money')
def test_withdrawal_of_money():
    """Withdrawal of money."""


@given('the account balance is 100')
def the_account_balance_is_100():
    """the account balance is 100."""
    pytest.AMT = 100


@when('the account holder withdraws 30')
def the_account_holder_withdraws_30():
    """the account holder withdraws 30."""
    pytest.AMT = 100


@then('the account balance should be 70')
def the_account_balance_should_be_70():
    """the account balance should be 70."""
    pytest.AMT = 100


def getInputCommands(inputCommandPath):
    number = []
    with open(inputCommandPath) as f:
        lines = f.read().splitlines()
        number = lines

    return number

def calculateMoving(commands):
    horizontal = 0
    depth = 0
    print(commands)
    for command in commands:
        order = command.split()[0]
        position = command.split()[1]
        if(order == "forward"):
            horizontal = horizontal + int(position)
        elif(order == "down"):
            depth = depth +  int(position)
        elif(order == "up"):
            depth = depth - int(position)

    return [horizontal, depth]


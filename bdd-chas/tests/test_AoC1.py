from pytest_bdd import scenario, given, then, when

from AoCDay1 import  getMeasurement, getResultString, loopInInputs


@scenario('../features/AoCDay1.feature', 'increase the depth')
def test_increase_the_depth():
    """calculate the increasing of depth """

@given('a int array of depth', target_fixture="input")
def a_int_array_of_depth():
    return input


@when('the previous is less than current number')
def the_previous_is_less_than_current_number():
    data = ['30', '40', '20', '10']
    loopInInputs(data)


@then('show increased depth')
def show_increased_depth():
    increases = getResultString(True)
    assert increases == "(increased)"

@then('return True')
def return_True():
    result = getMeasurement(40,30)
    assert result == True


@scenario('../features/AoCDay1.feature', 'decreased the depth')
def test_decreased_the_depth():
    """calculate the decreasing of depth."""

@given('a int array of depth', target_fixture="input")
def a_int_array_of_depth():
    return input


@when('the previous is greater than current number')
def the_previous_is_less_than_current_number():
    data = ['30', '40', '20', '10']
    loopInInputs(data)

@then('show decreased depth')
def show_increased_depth():
    decreased = getResultString(False)
    assert decreased == "(decreased)"

@then('return False')
def return_True():
    result = getMeasurement(20,40)
    assert result == False




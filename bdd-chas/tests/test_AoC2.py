from pytest_bdd import scenario, given, then, when

from AoCDay2 import calculateMoving


@scenario('../features/AoCDay2.feature', 'calculate the horizontal steps')
def test_calculate_the_horizontal_steps():
    """calculate the horizontal steps """

@given('a int array of commands', target_fixture="input")
def a_int_array_of_commands():
    return input


@when('the command is forward with number')
def the_command_is_forward_with_number():
    data = ['forward 9', 'down 9','up 4','down 5','down 6','up 6','down 7','down 1','forward 6']
    return data


@then('the submarine is moving horizontal with number of steps')
def the_submarine_is_moving_horizontal_with_number_of_steps():
    data = ['forward 9', 'down 9','up 4','down 5','down 6','up 6','down 7','down 1','forward 6']
    horizontal = calculateMoving(data)
    assert horizontal[0] == 15




@scenario('../features/AoCDay2.feature', 'calculate the depth steps')
def test_calculate_the_depth_steps():
    """calculate the depth steps"""

@given('a int array of commands', target_fixture="input")
def a_int_array_of_commands():
    return input


@when('the command is down or up with number')
def the_command_is_down_or_up_with_number():
    data = ['forward 9', 'down 9','up 4','down 5','down 6','up 6','down 7','down 1','forward 6']
    return data


@then('the submarine is increasing or decreasing the depth with number of steps')
def the_submarine_is_increasing_or_decreasing_the_depth_with_number_of_steps():
    data = ['forward 9', 'down 9','up 4','down 5','down 6','up 6','down 7','down 1','forward 6']
    horizontal = calculateMoving(data)
    assert horizontal[1] == 18
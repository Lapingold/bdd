def getInput(inputPath):
    numbers = []
    with open(inputPath) as f:
        lines = f.read().splitlines()
        numbers = lines

    return numbers

def getMeasurement(newNumber, oldNumber):
    if(newNumber > oldNumber):
        return True
    else:
        return False

def getResultString(result):
    if(result):
        return "(increased)"
    else:
        return "(decreased)"

def loopInInputs(numbers):
    resultList = []
    for number in range(len(numbers)):
        if(number == 0):
            resultList.append(numbers[number] + " " + "(N/A - no previous measurement)")
        else:
            result = getMeasurement(int(numbers[number]), int(numbers[number - 1]))
            resultList.append(numbers[number] + " " + getResultString(result))
    return resultList

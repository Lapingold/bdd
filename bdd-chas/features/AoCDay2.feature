Feature: AoC Day2

    A library to handle the measurement of moving for the submarine

    Scenario: calculate the horizontal steps
        Given a int array of commands
        When the command is forward with number 
        Then the submarine is moving horizontal with number of steps 



    Scenario: calculate the depth steps
        Given a int array of commands
        When the command is down or up with number 
        Then the submarine is increasing or decreasing the depth with number of steps 

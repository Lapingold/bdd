Feature: AoC Day1

    A library to handle the measurement depth for the submarine

    Scenario: increase the depth
        Given a int array of depth
        When the previous is less than current number
        Then return True
        Then show increased depth


    Scenario: decreased the depth
        Given a int array of depth
        When the previous is greater than current number
        Then return False
        Then show decreased depth
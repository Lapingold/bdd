
from AoCDay1 import getInput, loopInInputs
from AoCDay2 import calculateMoving, getInputCommands

def main():
    numbers = getInput("input.txt")
    loopInInputs(numbers)
    commands = getInputCommands("commands.txt")
    print(calculateMoving(commands))



if __name__ == "__main__":
    main()




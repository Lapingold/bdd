[![pipeline status](https://gitlab.com/majedsalem/bdd-chas/badges/main/pipeline.svg)](https://gitlab.com/majedsalem/bdd-chas/-/commits/main)
[![coverage report](https://gitlab.com/majedsalem/bdd-chas/badges/main/coverage.svg)](https://gitlab.com/majedsalem/bdd-chas/-/commits/main)

AoC Test 2021

https://adventofcode.com/2021/day/1
https://adventofcode.com/2021/day/2 


BDD VS TDD 

BDD, enligt min mening, är en metod för icke-programmerare. 
Det är bra när man jobbar i team med persnoer från verksamhet.

Som ett resultat är TDD mer användbar för att öka kodens kvalitet jämfört med BDD när det gäller funktionalitet.

verksamhet kan använda regressionstestning för att testa programvaran lättare än BDD. 

Från min erfarenhet bryr verksamhet mer om en buggfri version än att titta på och förstå koden